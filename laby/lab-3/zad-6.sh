#!/bin/bash
groupadd grupa1 && useradd -m -g grupa1 user1
groupadd grupa2 && useradd -m -g grupa2 user2
groupadd grupa3 && useradd -m -g grupa3 user3

mkdir /home/user1/Pulpit1
mkdir /home/user2/Pulpit2
mkdir /home/user3/Pulpit3
mkdir /home/user1/Pulpit1/katalog1
mkdir /home/user2/Pulpit2/katalog2
mkdir /home/user3/Pulpit3/katalog3
touch /home/user1/Pulpit1/plik_info1
touch /home/user2/Pulpit2/plik_info2
touch /home/user3/Pulpit3/plik_info3

chown -R user1:grupa1 /home/user1
chown -R user2:grupa2 /home/user2
chown -R user3:grupa3 /home/user3

