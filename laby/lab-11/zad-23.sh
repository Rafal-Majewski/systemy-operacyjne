#!/bin/bash
STARTING_DIR=$(pwd)

while true; do
	echo "Podaj nazwe uzytkownika do usuniecia:"
	read username
	if [[ -z $username ]]; then
		break
	fi
	if [[ "root student" =~ $username ]]; then
		echo "Nie mozna usunac uzytkownika $username"
		continue
	fi
	if ! [[ $(cat /etc/passwd | grep -o '^[^:]*' | grep $username) ]]; then
		echo "Nie ma takiego uzytkownika"
		continue
	fi
	userdel -r $username && echo "Usunieto uzytkownika $username" || echo "Nie udalo sie usunac uzytkownika $username"
done

while true; do
	echo "Podaj nazwe uzytkownika do stworzenia:"
	read username
	if [[ -z $username ]]; then
		break
	fi
	if [[ $(cat /etc/passwd | grep -o '^[^:]*' | grep $username) ]]; then
		echo "Uzytkownik $username juz istnieje"
		continue
	fi
	echo "Podaj nazwe grupy do ktorej ma byc dodany uzytkownik:"
	read groupname
	if [[ -z $groupname ]]; then
		break
	fi
	if [[ $(cat /etc/group | grep -o '^[^:]*' | grep $groupname) ]]; then
		echo "Grupa $groupname juz istnieje"
		continue
	fi
	groupadd $groupname && useradd -m -g $groupname $username && echo "Uzytkownik $username i grupa $gropuname stworzone" || echo "Nie udalo sie stworzyc uzytkownika $username i grupy $groupname"
done

tree /home

mkdir /home/nowy_użytkownik
mkdir /home/nowy_użytkownik/kat1
mkdir /home/nowy_użytkownik/backup
mkdir /home/nowy_użytkownik/archiwum
mkdir /home/nowy_użytkownik/kat1/kat2
touch /home/nowy_użytkownik/kat1/plik1

echo "kat1 zostanie zaraz zarchiwizowany! Uwaga!"
echo "Podaj nazwe archiwum do stworzenia:"
read filename

if [[ -e /home/nowy_użytkownik/archiwum/$filename ]]; then
	echo "Plik $filename juz istnieje w katalogu backup"
	exit 1
fi

tar -C /home/nowy_użytkownik -cf /home/nowy_użytkownik/archiwum/$filename kat1

cd /home/nowy_użytkownik
tar -tf ./archiwum/$filename
cd $STARTING_DIR

chown -R user:root /home/nowy_użytkownik


ls -Rl /home/nowy_użytkownik

chmod -R 660 /home/nowy_użytkownik
chown -R user:root /home/nowy_użytkownik

cp /home/nowy_użytkownik/archiwum/$filename /home/nowy_użytkownik/backup/

rm -r /home/nowy_użytkownik/kat1
rm -r /home/nowy_użytkownik/archiwum

# unpack the tar
tar -C /home/nowy_użytkownik -xf /home/nowy_użytkownik/backup/$filename

tree /home/nowy_użytkownik
