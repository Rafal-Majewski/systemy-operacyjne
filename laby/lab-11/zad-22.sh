#!/bin/bash
for username in $(cat /etc/passwd | grep -o '^[^:]*'| grep -v -e 'root' -e 'student'); do
	userdel -r $username
done

for groupname in $(cat /etc/group | grep -o '^[^:]*' | grep -v -e 'root' -e 'student'); do
	groupdel $groupname
done

groupadd grupa1 && useradd -m -g grupa1 user1
groupadd grupa2 && useradd -m -g grupa2 user2
groupadd grupa3 && useradd -m -g grupa3 user3

mkdir /home/user1/Pulpit1
mkdir /home/user1/Pulpit1/katalog1
mkdir /home/user2/Pulpit2
mkdir /home/user2/Pulpit2/katalog2
mkdir /home/user3/Pulpit3
mkdir /home/user3/Pulpit3/katalog3


tree /home

userdel -r user1
userdel -r user2
userdel -r user3
groupdel grupa1
groupdel grupa2
groupdel grupa3
