#!/bin/bash
function userExists() {
	getent passwd $1 1> /dev/null 2> /dev/null
}
function groupExists() {
	getent group $1 1> /dev/null 2> /dev/null
}

if userExists 'majewski1'; then
	userdel -r 'majewski1'
fi
if userExists 'majewski2'; then
	userdel -r 'majewski2'
fi
if userExists 'majewski3'; then
	userdel -r 'majewski3'
fi
if groupExists 'majski1'; then
	groupdel 'majewski1'
fi
if groupExists 'majski2'; then
	groupdel 'majewski2'
fi
if groupExists 'majski3'; then
	groupdel 'majewski3'
fi
groupadd 'majewski1'
groupadd 'majewski2'
groupadd 'majewski3'
useradd -m -g 'majewski1' 'majewski1'
useradd -m -g 'majewski2' 'majewski2'
useradd -m -g 'majewski3' 'majewski3'

tree /home

for i in {1..3}; do
	mkdir /home/majewski$i/kat_abc1
	mkdir /home/majewski$i/kat_abc1/kat_abc2
	touch /home/majewski$i/kat_abc1/kat_abc2/plik1
	mkdir /home/majewski$i/kat_abc3
	touch /home/majewski$i/kat_abc3/plik2
done
tree /home

mkdir /backup
for i in {1..3}; do
	tar -C /home/majewski$i -cf /backup/archiwum_majewski$i.tar kat_abc1
done
tree /backup
for i in {1..3}; do
	tar -tf /backup/archiwum_majewski$i.tar
done