#!/bin/bash
cat /etc/passwd | grep -o '^[^:]*'| grep -v -e 'root' -e 'student' | xargs --no-run-if-empty -L1 userdel -r
cat /etc/group | grep -o '^[^:]*' | grep -v -e 'root' -e 'student' | xargs --no-run-if-empty -L1 groupdel

username=""
groupname=""
echo "Podaj nazwe uzytkownika"
read username
echo "Podaj nazwe grupy"
read groupname

if ! [[ $(grep "^$username:" /etc/passwd) && $(grep "^$groupname:" /etc/group) ]] ; then
	groupadd $groupname
	useradd -g $groupname -m $username
	mkdir /home/$username/Pulpit
	touch /home/$username/Pulpit/plik_info1
	mkdir /home/$username/Pulpit/nowy_katalog
	touch /home/$username/Pulpit/nowy_katalog/nowy_plik
	tree /home/$username
	userdel -r $username
	groupdel $groupname
else
	echo "Uzytkownik lub grupa o podanej nazwie juz istnieja"
fi