#!/bin/bash
groupadd gr1
groupadd gr2
groupadd gr3

useradd -m -g gr1 user1
useradd -m -g gr2 user2
useradd -m -g gr3 user3

mkdir /home/user1/kat1
mkdir /home/user1/kat5
touch /home/user1/kat5/plik_info1
mkdir /home/user1/kat5/kat2
touch /home/user1/kat5/kat2/info2
mkdir /home/user1/kat5/kat2/kat3

mkdir /home/user2/kat3
touch /home/user2/kat3/plik_info3
mkdir /home/user2/kat1
mkdir /home/user2/kat1/kat2
touch /home/user2/kat1/plik_info2
mkdir /home/user2/kat1/kat2/kat5

mkdir /home/user3/kat5
touch /home/user3/kat5/plik_info3
touch /home/user3/kat5/plik_info2
mkdir /home/user3/kat6
mkdir /home/user3/kat6/kat
touch /home/user3/kat6/plik_info4
mkdir /home/user3/kat6/kat/kat1


tree /home

mkdir /Archiwum-user1
mkdir /Archiwum-user2
mkdir /Archiwum-user3
mkdir /backup

tar -cf /Archiwum-user1/user1.tar -C /home ./user1
tar -cf /Archiwum-user2/user2.tar -C /home ./user2
tar -cf /Archiwum-user3/user3.tar -C /home ./user3

tar -tf /Archiwum-user1/user1.tar
tar -tf /Archiwum-user2/user2.tar
tar -tf /Archiwum-user3/user3.tar

tar -xf /Archiwum-user1/user1.tar -C /backup/
tar -xf /Archiwum-user2/user2.tar -C /backup/
tar -xf /Archiwum-user3/user3.tar -C /backup/

userdel -r user1
userdel -r user2
userdel -r user3
groupdel gr1
groupdel gr2
groupdel gr3
