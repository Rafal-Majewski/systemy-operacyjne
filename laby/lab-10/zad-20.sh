#!/bin/bash
cat /etc/passwd | grep -o '^[^:]*'| grep -v -e "root" -e "student" | xargs --no-run-if-empty -L1 userdel -r
cat /etc/group | grep -o '^[^:]*' | grep -v -e "root" -e "student" | xargs --no-run-if-empty -L1 groupdel

username=""
groupname=""
echo "Podaj nazwe uzytkownika"
read username
echo "Podaj nazwe grupy"
read groupname

if ! [[ $(grep "^$username:" /etc/passwd) && $(grep "^$groupname:" /etc/group) ]] ; then
	groupadd $groupname
	useradd -g $groupname -m $username
	mkdir /home/$username/Pulpit
	touch /home/$username/Pulpit/plik_info1


	newdirectory=""
	echo "Podaj nazwe katalogu"
	read newdirectory
	if [[ -e /home/$username/Pulpit/$newdirectory ]] ; then
		echo "Katalog juz istnieje"

	else
		mkdir /home/$username/Pulpit/$newdirectory
		newfile=""
		echo "Podaj nazwe pliku"
		read newfile
		if [[ -e /home/$username/Pulpit/$newdirectory/$newfile ]] ; then
			echo "Plik juz istnieje"
		else
			touch /home/$username/Pulpit/$newdirectory/$newfile
		fi
	fi

	tree /home/$username

	userdel -r $username
	groupdel $groupname
else
	echo "Uzytkownik lub grupa o podanej nazwie juz istnieja"
fi