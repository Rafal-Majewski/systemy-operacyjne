#!/bin/bash

touch plik
chown student:student plik
chmod u=rw,g=r,o=rwx plik

while true
do
	echo "Zmień prawa właściciela do pliku."
	echo "1 - pokaż istniejące prawa do pliku"
	echo "2 - zmień na: rwx"
	echo "3 - zmień na: rw"
	echo "4 - zmień na: r--"
	echo "5 - zmień na: ---"
	echo "6 - wyjście"
	echo ""
	echo "Wybierz opcję?"


	read choice
	case $choice in
		"1")
			ls -l plik
			;;
		"2")
			chmod u=rwx plik
			;;
		"3")
			chmod u=rw plik
			;;
		"4")
			chmod u=r plik
			;;
		"5")
			chmod u-rwx plik
			;;
		"6")
			break
			;;
		*)
			echo "Nie ma takiej opcji"
			;;
	esac
done 