#!/bin/bash
createUserAndGroup() {
	local username="$1"
	local groupname="$2"
	groupadd $groupname
	useradd -m -g $groupname $username
}

echo "Podaj nazwe uzytkownika:"
read username
echo "Podaj nazwe grupy:"
read groupname
if [[ ! $(cat /etc/passwd | grep -o '^[^:]*' | grep $username) && ! $(cat /etc/group | grep -o '^[^:]*' | grep $groupname) ]]; then
	createUserAndGroup $username $groupname
	echo -n "" > /root/zad-24-log.txt
	echo "użytkownik: $username" >> /root/zad-24-log.txt
	echo "grupa: $groupname" >> /root/zad-24-log.txt
	ls -l /home
	userdel -r $username
	groupdel $groupname
	rm /root/zad-24-log.txt
else
	echo "Uzytkownik $username lub grupa $groupname istnieja"
	exit 1
fi