#!/bin/bash
while true
do
	echo "1 - podaj dane"
	echo "2 - sprawdzenie"
	echo "3 - dodaj konto"
	echo "4 - usun konto"
	echo "5 - wyjście"
	read choice
	case $choice in
		"1")
			echo "Podaj nazwe uzytkownika:"
			read username
			echo "Podaj nazwe grupy:"
			read groupname
			;;
		"2")
			if [[ $(cat /etc/passwd | grep -o '^[^:]*' | grep $username) && $(cat /etc/group | grep -o '^[^:]*' | grep $groupname) ]]; then
				echo "Uzytkownik $username i grupa $groupname istnieja"
			else
				echo "Uzytkownik $username lub grupa $groupname nie istnieja"
			fi
			;;
		"3")
			if [[ ! $(cat /etc/passwd | grep -o '^[^:]*' | grep $username) && ! $(cat /etc/group | grep -o '^[^:]*' | grep $groupname) ]]; then
				groupadd $groupname
				useradd -m -g $groupname $username
				ls -l /home
			else
				echo "Uzytkownik $username lub grupa $groupname istnieja"
			fi
			;;
		"4")
			userdel -r $username
			groupdel $groupname
			;;
		"5")
			break
			;;
		*)
			echo "Nie ma takiej opcji"
			;;
	esac
done 