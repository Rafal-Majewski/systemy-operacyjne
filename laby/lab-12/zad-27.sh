#!/bin/bash
while true
do	
	echo "Podaj nazwe uzytkownika:"
	read username
	echo "Podaj nazwe grupy:"
	read groupname
	if [[ $(cat /etc/passwd | grep -o '^[^:]*' | grep $username) && $(cat /etc/group | grep -o '^[^:]*' | grep $groupname) ]]; then
		echo "Uzytkownik $username i grupa $groupname istnieja"
	else
		echo "Uzytkownik $username lub grupa $groupname nie istnieja"
	fi
	if [[ ! $(cat /etc/passwd | grep -o '^[^:]*' | grep $username) && ! $(cat /etc/group | grep -o '^[^:]*' | grep $groupname) ]]; then
		groupadd $groupname
		useradd -m -g $groupname $username
		mkdir /home/$username/Pulpit
		mkdir /home/$username/kat_Test


		while true
		do
			echo "1 - nowy_katalog"
			echo "2 - nowy_plik"
			echo "3 - pokaż strukturę"
			echo "4 - wyjście"

			read choice
			case $choice in
				"1")
					mkdir /home/$username/Pulpit/nowy_katalog
					;;
				"2")
					touch /home/$username/Pulpit/nowy_katalog/nowy_plik
					;;
				"3")
					ls /home/$username
					;;
				"4")
					userdel -r $username
					groupdel $groupname
					exit 0
					;;
				*)
					echo "Nie ma takiej opcji"
					;;
			esac
		done 

	else
		echo "Uzytkownik $username lub grupa $groupname istnieja"
		echo "Czy chcesz ponownie wykonac skrypt? (t/n)"
		read choice
		if [[ $choice == "t" ]]; then
			continue
		else
			exit 0
		fi
	fi
done
