#!/bin/bash

function userExists() {
	getent passwd $1 1> /dev/null 2> /dev/null
}
function groupExists() {
	getent group $1 1> /dev/null 2> /dev/null
}

while true
do
	echo "1 - pokaż użytkowników"
	echo "2 - struktura zasobów"
	echo "3 - prawa dostępu"
	echo "x - wyjście ze skryptu"


	read choice
	case $choice in
		"1")
			baseGroupname="Majewski_gr"
			baseUsername="Majewski_user_"
			# na początku trzeba usunąć userów
			# bo nie można usunąć grup, które są primary
			# dla jakichś użytkowników
			for i in {1..3}; do
				username=$baseUsername$i
				if userExists $username; then
					userdel -r $username
				fi
			done
			# usuwanie i tworzenie grup
			for i in {1..3}; do
				groupname=$baseGroupname$i
				if groupExists $groupname; then
					groupdel $groupname
				fi
				groupadd $groupname
			done
			# tworzenie użytkowników
			for i in {1..3}; do
				username="Majewski_user_$i"
				if userExists $username; then
					userdel -r $username
				fi
				useradd -m -g $baseGroupname$(("4 - $i")) $username
			done
			ls -l /home
			;;
		"2")
			for i in {1..3}; do
				username="Majewski_user_$i"
				if ! userExists $username; then
					echo "Uzytkownik $username nie istnieje!"
					continue
				fi
				baseFilepath="/home/$username"
				mkdir $baseFilepath/kat1
				mkdir $baseFilepath/kat1/kat2
				mkdir $baseFilepath/kat1/kat2/kat3
				touch $baseFilepath/kat1/plik3
				touch $baseFilepath/kat1/plik1
				touch $baseFilepath/plik2
			done
			tree /home
			;;
		"3")
			echo "Podaj cyfrę od 3 do 5:"
			read permissionsDigit
			if [[ $permissionsDigit =~ ^[3-5]$ ]]; then
				echo "Podaj ścieżkę do katalogu:"
				read filepath
				if [[ -d $filepath ]]; then
					stat -c "Stare prawa dostępu: %a" $filepath
					chmod -R $permissionsDigit`stat -c %a $filepath | cut -c2-3` $filepath
					stat -c "Nowe prawa dostępu: %a" $filepath
				else
					if [[ $filepath == "" ]]; then
						echo "Nie podano ścieżki!"
					else
						echo "Nie ma takiego katalogu!"
					fi
				fi
			else
				echo "Niepoprawna cyfra!"
			fi
			;;
		"x")
			break
			;;
		*)
			echo "Nie ma takiej opcji!"
			;;
	esac
	echo ""
done 