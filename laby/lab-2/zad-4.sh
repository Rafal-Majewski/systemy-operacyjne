#!/bin/bash
rm -rf ~/Pulpit/*
mkdir ~/Pulpit/test_a ~/Pulpit/test_b ~/Pulpit/test_c
echo "a" > ~/Pulpit/test_a/plik_a ; echo "b" > ~/Pulpit/test_b/plik_b ; echo "c" > ~/Pulpit/test_c/plik_c
CURRENT_DIR=$(pwd) ; mkdir ./Pulpit/kopia ; cd ~ ; find ./Pulpit -maxdepth 1 -mindepth 1 ! -wholename "./Pulpit/kopia" -exec cp -r {} ./Pulpit/kopia/ \; ; cd $CURRENT_DIR
mkdir ~/Pulpit/move1
mv ~/Pulpit/test_a ~/Pulpit/move1/
mv ~/Pulpit/test_b ~/Pulpit/move1/
mv ~/Pulpit/test_c ~/Pulpit/move1/
tree ~/Pulpit ; find ~/Pulpit/* -type f -exec cat {} \;
