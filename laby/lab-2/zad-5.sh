#!/bin/bash
CURRENT_DIR=$(pwd)

mkdir ./Pulpit
mkdir ./Pulpit/test1 ./Pulpit/test2 ./Pulpit/kopia
cd $CURRENT_DIR/Pulpit/test2 ; mkdir $CURRENT_DIR/Pulpit/test1/test3
cd $CURRENT_DIR/Pulpit/test1/test3 ; touch $CURRENT_DIR/Pulpit/test1/info1
cd $CURRENT_DIR/Pulpit/kopia ; touch $CURRENT_DIR/Pulpit/test1/info2
cd $CURRENT_DIR/Pulpit/test2 ; cp -r $CURRENT_DIR/Pulpit/test1/* $CURRENT_DIR/Pulpit/kopia/
cd $CURRENT_DIR/Pulpit/kopia ; rm -f $CURRENT_DIR/Pulpit/test1/info1 $CURRENT_DIR/Pulpit/test1/info2
cd $CURRENT_DIR/Pulpit/test1 ; mv -t ./ $CURRENT_DIR/Pulpit/kopia/info1 $CURRENT_DIR/Pulpit/kopia/info2
cd ~ ; tree $CURRENT_DIR/Pulpit ; cat $CURRENT_DIR/Pulpit/test1/info1 ; cat $CURRENT_DIR/Pulpit/test1/info2

cd $CURRENT_DIR