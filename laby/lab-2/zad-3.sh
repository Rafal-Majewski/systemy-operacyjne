#!/bin/bash
CURRENT_DIR=$(pwd)
mkdir ~/Pulpit
mkdir ~/Pulpit/test
echo "Rafał Majewski" > ~/Pulpit/test/info1
date > ~/Pulpit/test/info2
mkdir ~/Pulpit/kopia
cd ~/Pulpit/test
cp info2 ../kopia/
cd ~/Pulpit/kopia
mv info2 info3
echo "INFORMATYKA" > info3
cp info3 ../test/
mv ../test/info3 ../test/info4
cd ~
tree Pulpit
cat Pulpit/test/info1
cat Pulpit/test/info4
cd $CURRENT_DIR
