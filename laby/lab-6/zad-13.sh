#!/bin/bash
if ! [[ $(grep "^us1:" /etc/passwd) || $(grep "^us2:" /etc/passwd) || $(grep "^us3:" /etc/passwd) || $(grep "^gr1:" /etc/group) || $(grep "^gr2:" /etc/group) || $(grep "^gr3:" /etc/group) ]]; then
	groupadd gr1
	groupadd gr2
	groupadd gr3
	useradd -g gr1 us1
	useradd -g gr2 us2
	useradd -g gr3 us3
else
	if [[ $(grep "^us1:" /etc/passwd) && $(grep "^us2:" /etc/passwd) && $(grep "^us3:" /etc/passwd) && $(grep "^gr1:" /etc/group) && $(grep "^gr2:" /etc/group) && $(grep "^gr3:" /etc/group) ]]; then
		userdel -r us1
		userdel -r us2
		userdel -r us3
		groupdel gr1
		groupdel gr2
		groupdel gr3
	fi
fi

mkdir /home/us1
mkdir /home/us1/kat1a
touch /home/us1/kat1a/plik1a
touch /home/us1/kat1a/plik1b
mkdir /home/us1/kat1a/kat1c
touch /home/us1/kat1a/kat1c/plik1
mkdir /home/us1/kat1b

mkdir /home/us2
mkdir /home/us2/kat1a
touch /home/us2/kat1a/plik1a
mkdir /home/us2/kat1c
touch /home/us2/kat1c/plik1
mkdir /home/us2/kat1b

mkdir /home/us3
mkdir /home/us3/kat1a
touch /home/us3/kat1a/plik1a
mkdir /home/us3/kat1c
touch /home/us3/plik1
mkdir /home/us3/kat1b

mkdir /home/student/archiwum
mkdir /home/student/rozpakowane


tar -cf /home/student/archiwum/us1.tar -C /home ./us1
tar -cf /home/student/archiwum/us2.tar -C /home ./us2
tar -cf /home/student/archiwum/us3.tar -C /home ./us3

CURRENT_DIR=$(pwd)
cd /home/student
tar -xf ./archiwum/us1.tar -C ./rozpakowane/
tar -xf ./archiwum/us2.tar -C ./rozpakowane/
tar -xf ./archiwum/us3.tar -C ./rozpakowane/
cd $CURRENT_DIR

tree /home/student