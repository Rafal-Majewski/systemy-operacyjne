#!/bin/bash

mkdir /home/student/kat1
mkdir /home/student/kat1/kat1a
touch /home/student/kat1/kat1a/plik1a
mkdir /home/student/kat1/kat1b
touch /home/student/kat1/kat1b/plik1b
touch /home/student/kat1/plik1

mkdir /home/student/kopia
mkdir /home/student/archiwum
mkdir /home/student/rozpakowane

tree /home/student


CURRENT_DIR=$(pwd)
cd /home/student/kat1/kat1b
cp -r ../.. ../../kopia/
cd /home/student
tar -cf ./archiwum/kat1_arch.tar ./kat1
cd /home/student/rozpakowane
tar -tf ../archiwum/kat1_arch.tar
cd /home/student/kat1/kat1b
tar -xf ../../archiwum/kat1_arch.tar -C ../../rozpakowane && rm -rf ../../kopia/*

cd $CURRENT_DIR