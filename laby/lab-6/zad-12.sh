#!/bin/bash
rm -rf /home/student/*

mkdir /home/student/kat1
mkdir /home/student/kat1/kat1a
touch /home/student/kat1/kat1a/plik1a
touch /home/student/kat1/kat1a/plik1b
mkdir /home/student/kat1/kat1a/kat1c
touch /home/student/kat1/plik1
mkdir /home/student/kat1/kat1b

mkdir /home/student/kopia
mkdir /home/student/archiwum
mkdir /home/student/rozpakowane

tree /home/student

cd /home/student/kat1/kat1a/kat1c
cp -r ../../../kat1 ../../../kopia/
cd /home/student
tar -cf ./archiwum/kat1_arch.tar ./kat1
cd /home/student/kat1/kat1a/kat1c
tar -tf ../../../archiwum/kat1_arch.tar
cd /home/student/kat1/kat1b
tar -xf ../../archiwum/kat1_arch.tar -C ../../rozpakowane && rm -rf ../../kopia/*

tree /home/student