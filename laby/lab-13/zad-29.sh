#!/bin/bash
# read username and groupname
# if the user and group do not exist, the script will create a group, account and assign it
# group account; otherwise, the script will return an appropriate message

echo "Podaj nazwe uzytkownika:"
read username
echo "Podaj nazwe grupy:"
read groupname
if [[ ! $(cat /etc/passwd | grep -o '^[^:]*' | grep $username) && ! $(cat /etc/group | grep -o '^[^:]*' | grep $groupname) ]]; then
	groupadd $groupname
	useradd -m -g $groupname $username
	mkdir /home/$username/Pulpit1
	mkdir /home/$username/Pulpit1/kat1
	touch /home/$username/Pulpit1/kat1/plik1
	touch /home/$username/Pulpit1/kat1/plik2
	touch /home/$username/Pulpit1/kat1/plik3
	mkdir /home/$username/Pulpit1/kat1/kat2
	touch /home/$username/Pulpit1/kat1/kat2/plik4

	mkdir /home/$username/Pulpit2
	mkdir /home/$username/Pulpit2/kat1
	touch /home/$username/Pulpit2/kat1/plik1
	touch /home/$username/Pulpit2/kat1/plik2
	mkdir /home/$username/Pulpit2/kat1/kat2
	touch /home/$username/Pulpit2/kat1/kat2/plik4
	touch /home/$username/Pulpit2/kat1/kat2/plik3
	echo "Podaj nazwe archiwum 1:"
	read archive1
	echo "Podaj nazwe archiwum 2:"
	read archive2
	mkdir /home/$username/backup
	tar -cf /home/$username/backup/$archive1 -C /home/$username Pulpit1
	tar -cf /home/$username/backup/$archive2 -C /home/$username Pulpit2
	echo "Podaj nazwe archiwum do podejrzenia:"
	read archive
	tar -tf /home/$username/backup/$archive
	rm /home/$username/backup/$archive1
	echo "Podaj nazwe archiwum 1:"
	read archive1
	tar -tf /home/$username/backup/$archive1 || echo "Nie ma takiego archiwum"
	mkdir /home/$username/rozpakowane
	echo "Podaj nazwe archiwum 1:"
	tar -cf /home/$username/backup/$archive1 -C /home/$username Pulpit1
	tar -xf /home/$username/backup/$archive1 -C /home/$username/rozpakowane
	tar -xf /home/$username/backup/$archive2 -C /home/$username/rozpakowane
	tree /home/$username/rozpakowane
else
	echo "Uzytkownik $username lub grupa $groupname istnieja"
fi