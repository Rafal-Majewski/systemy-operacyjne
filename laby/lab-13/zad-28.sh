#!/bin/bash
if [[ $# -ne 1 ]]; then
	echo "Podaj jedna sciezke do pliku"
	exit 1
fi
filepath=$1
echo "Prawa dostepu wlasciciela: `stat -c %A $filepath | cut -c1-3`"
