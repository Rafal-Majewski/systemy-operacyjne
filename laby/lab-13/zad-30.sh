#!/bin/bash
echo "Podaj nazwe uzytkownika:"
read username
echo "Podaj nazwe grupy:"
read groupname
if [[ ! $(cat /etc/passwd | grep -o '^[^:]*' | grep $username) && ! $(cat /etc/group | grep -o '^[^:]*' | grep $groupname) ]]; then
	groupadd $groupname
	useradd -m -g $groupname $username
	mkdir /home/$username/kat1
	touch /home/$username/kat1/plik1
	touch /home/$username/kat1/plik2
	mkdir /home/$username/kat1/kat2
	touch /home/$username/kat1/kat2/plik4
	mkdir /home/$username/kat3
	touch /home/$username/plik3
	chown -R $username:$groupname /home/$username
	echo "Podaj cyfre dostepu do plikow dla uzytkownika $username:"
	read access
	chmod -R $access`stat -c %a /home/$username | cut -c2-3` /home/$username
	echo "Podaj cyfre dostepu do plikow dla grupy $groupname:"
	read access
	chmod -R `stat -c %a /home/$username | cut -c1-1`$access`stat -c %a /home/$username | cut -c3-3` /home/$username
	echo "Podaj cyfre dostepu do plikow dla innych uzytkownikow:"
	read access
	chmod -R `stat -c %a /home/$username | cut -c1-2`$access /home/$username
	stat -c %a /home/$username
	find /home/$username -type f -exec ls -l {} \;

	echo "Podaj cyfre dostepu do plikow dla uzytkownika $username:"
	read access
	chmod -R $access`stat -c %a /home/$username | cut -c2-3` /home/$username
	echo "Podaj cyfre dostepu do plikow dla grupy $groupname:"
	read access
	chmod -R `stat -c %a /home/$username | cut -c1-1`$access`stat -c %a /home/$username | cut -c3-3` /home/$username
	echo "Podaj cyfre dostepu do plikow dla innych uzytkownikow:"
	read access
	chmod -R `stat -c %a /home/$username | cut -c1-2`$access /home/$username
	find /home/$username -type f -exec ls -l {} \;
	
else
	echo "Uzytkownik $username lub grupa $groupname istnieja"
fi